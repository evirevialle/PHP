# PHP

This repository shows differents PHP functions to see a lots of images and to save informations of people, street and companies on Zotero.  
The functions were created for project *Adressbuch* of [Institut Historique Allemand de Paris](https://www.dhi-paris.fr/fr/page-daccueil.html).  
The project _Adressbuch_ [**here !**](https://github.com/dhi-digital-humanities/Adressbuch)
